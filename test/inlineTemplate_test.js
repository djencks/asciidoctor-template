/* eslint-env mocha */

// IMPORTANT eagerly load Opal since we'll always be in this context; change String encoding from UTF-16LE to UTF-8
const { Opal } = require('asciidoctor-opal-runtime')
if ('encoding' in String.prototype && String(String.prototype.encoding) !== 'UTF-8') {
  String.prototype.encoding = Opal.const_get_local(Opal.const_get_qualified('::', 'Encoding'), 'UTF_8') // eslint-disable-line
}
const asciidoctor = require('@asciidoctor/core')()
const template = require('../lib/template')

const { expect } = require('chai')

describe('inline template tests', () => {
  beforeEach(() => {
    asciidoctor.Extensions.unregisterAll()
  })

  ;[{
    type: 'global',
    f: (text) => {
      template.register(asciidoctor.Extensions)
      return asciidoctor.convert(text)
    },
  },
  {
    type: 'registry',
    f: (text) => {
      const registry = template.register(asciidoctor.Extensions.create())
      return asciidoctor.convert(text, { extension_registry: registry })
    },
  }].forEach(({ type, f }) => {
    it(`template can be registered with defineTemplate, ${type}`, () => {
      const html = f(`
:var1: value of var1

[inlineTemplate,test,'var1,var2']
This is var1: {var1}.
This is var2: {var2}.

Defined!
`)
      expect(html).to.equal('<div class="paragraph">\n<p>Defined!</p>\n</div>')
    })

    it(`template can be found with applyTemplate, named parameters, ${type}`, () => {
      const html = f(`
:var1: value of var1

[inlineTemplate,test]
This is var1: {var1}.
This is var2: {var2}.

:var2: value of var2

// This is an intervening paragraph.

Inline test test:[var1=foo,var2=bar] after inline test.

var1: {var1}

var2: {var2}
`)
      expect(html).to.equal(`<div class="paragraph">
<p>Inline test This is var1: foo.
This is var2: bar. after inline test.</p>
</div>
<div class="paragraph">
<p>var1: value of var1</p>
</div>
<div class="paragraph">
<p>var2: value of var2</p>
</div>`)
    })

    it(`template can be found with applyTemplate, positional parameters, ${type}`, () => {
      const html = f(`
:var1: value of var1

[inlineTemplate,test,'var1,var2']
This is var1: {var1}.
This is var2: {var2}.

:var2: value of var2

// This is an intervening paragraph.

Inline test test:[foo,bar] after inline test.

var1: {var1}

var2: {var2}
`)
      expect(html).to.equal(`<div class="paragraph">
<p>Inline test This is var1: foo.
This is var2: bar. after inline test.</p>
</div>
<div class="paragraph">
<p>var1: value of var1</p>
</div>
<div class="paragraph">
<p>var2: value of var2</p>
</div>`)
    })

    it(`template can be found with applyTemplate, positional parameters, multiple uses, ${type}`, () => {
      const html = f(`
:var1: value of var1

[inlineTemplate,test,'var1,var2']
This is var1: {var1}.
This is var2: {var2}.

:var2: value of var2

// This is an intervening paragraph.

Inline test test:[foo,bar] after inline test.
Another test test:[xfoo,xbar] after another inline test.
`)
      expect(html).to.equal(`<div class="paragraph">
<p>Inline test This is var1: foo.
This is var2: bar. after inline test.
Another test This is var1: xfoo.
This is var2: xbar. after another inline test.</p>
</div>`)
    })

    it(`template can be found with applyTemplate, both positional and named parameters, ${type}`, () => {
      const html = f(`
:var1: value of var1

[inlineTemplate,test,'var1']
This is var1: {var1}.
This is var2: {var2}.

:var2: value of var2

// This is an intervening paragraph.

test:[foo,var2=bar]

var1: {var1}

var2: {var2}
`)
      expect(html).to.equal(`<div class="paragraph">
<p>This is var1: foo.
This is var2: bar.</p>
</div>
<div class="paragraph">
<p>var1: value of var1</p>
</div>
<div class="paragraph">
<p>var2: value of var2</p>
</div>`)
    })

    it(`template can be found with applyTemplate, unspecified non-positional attribute uses ambient value, ${type}`, () => {
      const html = f(`
:var1: value of var1

[inlineTemplate,test,'var1']
This is var1: {var1}.
This is var2: {var2}.

:var2: value of var2

// This is an intervening paragraph.

test:[foo]

var1: {var1}

var2: {var2}
`)
      expect(html).to.equal(`<div class="paragraph">
<p>This is var1: foo.
This is var2: value of var2.</p>
</div>
<div class="paragraph">
<p>var1: value of var1</p>
</div>
<div class="paragraph">
<p>var2: value of var2</p>
</div>`)
    })

    it(`template can be found with applyTemplate, unspecified positional attribute does not use ambient value, ${type}`, () => {
      const html = f(`
:var1: value of var1

[inlineTemplate,test,'var1']
This is var1: {var1}.
This is var2: {var2}.

:var2: value of var2

// This is an intervening paragraph.

test:[,var2=bar]

var1: {var1}

var2: {var2}
`)
      expect(html).to.equal(`<div class="paragraph">
<p>This is var1: {var1}.
This is var2: bar.</p>
</div>
<div class="paragraph">
<p>var1: value of var1</p>
</div>
<div class="paragraph">
<p>var2: value of var2</p>
</div>`)
    })

    it(`template with multiple blocks will produce no output and a warning (not checked), ${type}`, () => {
      const html = f(`
:var1: value of var1

[inlineTemplate,test,'var1,var2']
----
== Section {var1}

This is var1: {var1}.

== Section {var2}

This is var2: {var2}.
----

This is a first intervening paragraph

:var2: value of var2

// This is an intervening paragraph.

test:[foo,bar]

var1: {var1}

var2: {var2}
`)
      expect(html).to.equal(`<div class="paragraph">
<p>This is a first intervening paragraph</p>
</div>
<div class="paragraph">
<p></p>
</div>
<div class="paragraph">
<p>var1: value of var1</p>
</div>
<div class="paragraph">
<p>var2: value of var2</p>
</div>`)
    })
  })
})
