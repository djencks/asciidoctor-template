/* eslint-env mocha */

// IMPORTANT eagerly load Opal since we'll always be in this context; change String encoding from UTF-16LE to UTF-8
const { Opal } = require('asciidoctor-opal-runtime')
if ('encoding' in String.prototype && String(String.prototype.encoding) !== 'UTF-8') {
  String.prototype.encoding = Opal.const_get_local(Opal.const_get_qualified('::', 'Encoding'), 'UTF_8') // eslint-disable-line
}
const asciidoctor = require('@asciidoctor/core')()
const template = require('../lib/template')

const { expect } = require('chai')

describe('block template tests', () => {
  beforeEach(() => {
    asciidoctor.Extensions.unregisterAll()
  })

  ;[{
    type: 'global',
    f: (text) => {
      template.register(asciidoctor.Extensions)
      return asciidoctor.convert(text)
    },
  },
  {
    type: 'registry',
    f: (text) => {
      const registry = template.register(asciidoctor.Extensions.create())
      return asciidoctor.convert(text, { extension_registry: registry })
    },
  }].forEach(({ type, f }) => {
    it(`template can be registered with blockTemplate, ${type}`, () => {
      const html = f(`
:var1: value of var1

[blockTemplate,test,'var1,var2']
This is var1: {var1}.
This is var2: {var2}.

Defined!
`)
      expect(html).to.equal('<div class="paragraph">\n<p>Defined!</p>\n</div>')
    })

    it(`template can be found with applyTemplate, named parameters, ${type}`, () => {
      const html = f(`
:var1: value of var1

[blockTemplate,test]
This is var1: {var1}.
This is var2: {var2}.
Here is the content: {content}

:var2: value of var2

// This is an intervening paragraph.

[test,var1=foo,var2=bar]
Some Content

var1: {var1}

var2: {var2}
`)
      expect(html).to.equal(`<div class="paragraph">
<p>This is var1: foo.
This is var2: bar.
Here is the content: Some Content</p>
</div>
<div class="paragraph">
<p>var1: value of var1</p>
</div>
<div class="paragraph">
<p>var2: value of var2</p>
</div>`)
    })

    it(`template can be found with applyTemplate, positional parameters, ${type}`, () => {
      const html = f(`
:var1: value of var1

[blockTemplate,test,'var1,var2']
This is var1: {var1}.
This is var2: {var2}.
Here is the content: {content}

:var2: value of var2

// This is an intervening paragraph.

[test,foo,bar]
----
A long text content.
----

== section {var1}

var1: {var1}

== section {var2}

var2: {var2}
`)
      expect(html).to.equal(`<div class="paragraph">
<p>This is var1: foo.
This is var2: bar.
Here is the content: A long text content.</p>
</div>
<div class="sect1">
<h2 id="_section_value_of_var1">section value of var1</h2>
<div class="sectionbody">
<div class="paragraph">
<p>var1: value of var1</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_section_value_of_var2">section value of var2</h2>
<div class="sectionbody">
<div class="paragraph">
<p>var2: value of var2</p>
</div>
</div>
</div>`)
    })

    it(`template can be found with applyTemplate, positional parameters, multiple uses, ${type}`, () => {
      const html = f(`
:var1: value of var1

[blockTemplate,test,'var1,var2']
This is var1: {var1}.
This is var2: {var2}.
Here is the content: {content}

:var2: value of var2

// This is an intervening paragraph.

[test,foo,bar]
----
A long text content.
----

[test,xfoo,xbar]
----
A short text content.
----

`)
      expect(html).to.equal(`<div class="paragraph">
<p>This is var1: foo.
This is var2: bar.
Here is the content: A long text content.</p>
</div>
<div class="paragraph">
<p>This is var1: xfoo.
This is var2: xbar.
Here is the content: A short text content.</p>
</div>`)
    })

    it(`template can be found with applyTemplate, both positional and named parameters, ${type}`, () => {
      const html = f(`
:var1: value of var1

[blockTemplate,test,'var1']
This is var1: {var1}.
This is var2: {var2}.
Here is the content: {content}

:var2: value of var2

// This is an intervening paragraph.

[test,foo,var2=bar]
A short text sentence.
Another sentence.

var1: {var1}

var2: {var2}
`)
      expect(html).to.equal(`<div class="paragraph">
<p>This is var1: foo.
This is var2: bar.
Here is the content: A short text sentence.
Another sentence.</p>
</div>
<div class="paragraph">
<p>var1: value of var1</p>
</div>
<div class="paragraph">
<p>var2: value of var2</p>
</div>`)
    })

    it(`template can be found with applyTemplate, unspecified non-positional attribute uses ambient value, ${type}`, () => {
      const html = f(`
:var1: value of var1

[blockTemplate,test,'var1']
This is var1: {var1}.
This is var2: {var2}.
Here is the content:
{content}

:var2: value of var2

// This is an intervening paragraph.

[test,foo]
--
----
Some exciting content.
----
--

var1: {var1}

var2: {var2}
`)
      expect(html).to.equal(`<div class="paragraph">
<p>This is var1: foo.
This is var2: value of var2.
Here is the content:</p>
</div>
<div class="listingblock">
<div class="content">
<pre>Some exciting content.</pre>
</div>
</div>
<div class="paragraph">
<p>var1: value of var1</p>
</div>
<div class="paragraph">
<p>var2: value of var2</p>
</div>`)
    })

    it(`template can be found with applyTemplate, unspecified positional attribute does not use ambient value, ${type}`, () => {
      const html = f(`
:var1: value of var1

[blockTemplate,test,'var1']
This is var1: {var1}.
This is var2: {var2}.
Here is the content: {content}

:var2: value of var2

// This is an intervening paragraph.

[test,var2=bar]
Some exciting content.

var1: {var1}

var2: {var2}
`)
      expect(html).to.equal(`<div class="paragraph">
<p>This is var1: {var1}.
This is var2: bar.
Here is the content: Some exciting content.</p>
</div>
<div class="paragraph">
<p>var1: value of var1</p>
</div>
<div class="paragraph">
<p>var2: value of var2</p>
</div>`)
    })

    it(`template with multiple blocks, ${type}`, () => {
      const html = f(`
:var1: value of var1

[blockTemplate,test,'var1,var2']
----
== Section {var1}

This is var1: {var1}.

== Section {var2}

This is var2: {var2}.

Here is the content: {content}
----

This is a first intervening paragraph

:var2: value of var2

// This is an intervening paragraph.

[test,foo,bar]
Some exciting content.

== section {var1}

var1: {var1}

== section {var2}

var2: {var2}
`)
      expect(html).to.equal(`<div class="paragraph">
<p>This is a first intervening paragraph</p>
</div>
<div class="sect1">
<h2 id="_section_foo">Section foo</h2>
<div class="sectionbody">
<div class="paragraph">
<p>This is var1: foo.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_section_bar">Section bar</h2>
<div class="sectionbody">
<div class="paragraph">
<p>This is var2: bar.</p>
</div>
<div class="paragraph">
<p>Here is the content: Some exciting content.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_section_value_of_var1">section value of var1</h2>
<div class="sectionbody">
<div class="paragraph">
<p>var1: value of var1</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_section_value_of_var2">section value of var2</h2>
<div class="sectionbody">
<div class="paragraph">
<p>var2: value of var2</p>
</div>
</div>
</div>`)
    })

    it(`template with multiple blocks, unspecified non-positional attribute uses ambient value, ${type}`, () => {
      const html = f(`
:var1: value of var1

[blockTemplate,test,'var1']
----
== Section {var1}

This is var1: {var1}.

== Section {var2}

This is var2: {var2}.

Here is the content: {content}
----

This is a first intervening paragraph

:var2: value of var2

// This is an intervening paragraph.

[test,foo]
Some exciting content.

var1: {var1}

var2: {var2}
`)
      expect(html).to.equal(`<div class="paragraph">
<p>This is a first intervening paragraph</p>
</div>
<div class="sect1">
<h2 id="_section_foo">Section foo</h2>
<div class="sectionbody">
<div class="paragraph">
<p>This is var1: foo.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_section_value_of_var2">Section value of var2</h2>
<div class="sectionbody">
<div class="paragraph">
<p>This is var2: value of var2.</p>
</div>
<div class="paragraph">
<p>Here is the content: Some exciting content.</p>
</div>
</div>
</div>
<div class="paragraph">
<p>var1: value of var1</p>
</div>
<div class="paragraph">
<p>var2: value of var2</p>
</div>`)
    })

    it(`template with multiple blocks, unspecified positional attribute does not use ambient value, ${type}`, () => {
      const html = f(`
:var1: value of var1

[blockTemplate,test,'var1']
----
== Section {var1}

This is var1: {var1}.

== Section {var2}

This is var2: {var2}.

Here is the content: {content}
----

This is a first intervening paragraph

:var2: value of var2

// This is an intervening paragraph.

[test,,var2=bar]
Some exciting content.

var1: {var1}

var2: {var2}
`)
      expect(html).to.equal(`<div class="paragraph">
<p>This is a first intervening paragraph</p>
</div>
<div class="sect1">
<h2 id="_section_var1">Section {var1}</h2>
<div class="sectionbody">
<div class="paragraph">
<p>This is var1: {var1}.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_section_bar">Section bar</h2>
<div class="sectionbody">
<div class="paragraph">
<p>This is var2: bar.</p>
</div>
<div class="paragraph">
<p>Here is the content: Some exciting content.</p>
</div>
</div>
</div>
<div class="paragraph">
<p>var1: value of var1</p>
</div>
<div class="paragraph">
<p>var2: value of var2</p>
</div>`)
    })

    it(`template with multiple blocks, template attributes transfer between blocks, ${type}`, () => {
      const html = f(`
:var1: value of var1

[blockTemplate,test,'var1,var2']
----
:var3: baz

== Section {var1}

This is var1: {var1}.
This is var3: {var3}.

:var4: bax
// This is var4: {var4}.

== Section {var2}

This is var2: {var2}.
This is var4: {var4}.

Here is the content: {content}

== Section {var3}

This is var3: {var3}.
This is var4: {var4}.
----

This is a first intervening paragraph

:var2: value of var2

// This is an intervening paragraph.

[test,foo,bar]
----
Some exciting content.
----

var1: {var1}

var2: {var2}

//check that attributes defined in template don't leak out
var3: {var3}

var4: {var4}
`)
      expect(html).to.equal(`<div class="paragraph">
<p>This is a first intervening paragraph</p>
</div>
<div class="sect1">
<h2 id="_section_foo">Section foo</h2>
<div class="sectionbody">
<div class="paragraph">
<p>This is var1: foo.
This is var3: baz.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_section_bar">Section bar</h2>
<div class="sectionbody">
<div class="paragraph">
<p>This is var2: bar.
This is var4: bax.</p>
</div>
<div class="paragraph">
<p>Here is the content: Some exciting content.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_section_baz">Section baz</h2>
<div class="sectionbody">
<div class="paragraph">
<p>This is var3: baz.
This is var4: bax.</p>
</div>
</div>
</div>
<div class="paragraph">
<p>var1: value of var1</p>
</div>
<div class="paragraph">
<p>var2: value of var2</p>
</div>
<div class="paragraph">
<p>var3: {var3}</p>
</div>
<div class="paragraph">
<p>var4: {var4}</p>
</div>`)
    })

    it(`template with table, ${type}`, () => {
      const html = f(`
[blockTemplate,mytable,'term,priority']
----
[cols='3,2,5',separator=|]
|===
|*Term*
|Priority
|_Description_

|{term}
|{priority}
|{content}
|===
----

== A section with a table

[mytable,Earnestness,dramatic]
----
The importance of being
----

And this ends the document.

`)
      expect(html).to.equal(`<div class="sect1">
<h2 id="_a_section_with_a_table">A section with a table</h2>
<div class="sectionbody">
<table class="tableblock frame-all grid-all stretch">
<colgroup>
<col style="width: 30%;">
<col style="width: 20%;">
<col style="width: 50%;">
</colgroup>
<tbody>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock"><strong>Term</strong></p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">Priority</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock"><em>Description</em></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">Earnestness</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">dramatic</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">The importance of being</p></td>
</tr>
</tbody>
</table>
<div class="paragraph">
<p>And this ends the document.</p>
</div>
</div>
</div>`)
    })

    it(`template with open block for samples, ${type}`, () => {
      const html = f(`
[blockTemplate,sample]
--
[subs=+attributes]
----
{content}
----

{content}
--

== A section with a sample

[sample]
----
The importance of being
----

And this ends the document.

`)
      expect(html).to.equal(`<div class="sect1">
<h2 id="_a_section_with_a_sample">A section with a sample</h2>
<div class="sectionbody">
<div class="listingblock">
<div class="content">
<pre>The importance of being</pre>
</div>
</div>
<div class="paragraph">
<p>The importance of being</p>
</div>
<div class="paragraph">
<p>And this ends the document.</p>
</div>
</div>
</div>`)
    })
  })
})
