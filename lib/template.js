'use strict'

const Opal = global.Opal

const EMPTY = Opal.hash2([], {})

const $precedingAttributes = Symbol('precedingAttributes')
const $innerDoc = Symbol('innerDoc')

const inlineDocument = Opal.module(null, 'Asciidoctor').Document

const innerDocument = (() => {
  const scope = Opal.klass(
    Opal.module(null, 'Templates', function () {
    }),
    Opal.module(null, 'Asciidoctor').Document,
    'innerDocument',
    function () {
    }
  )
  Opal.defn(scope, '$initialize', function initialize (backend, opts, precedingAttributes) {
    Opal.send(this, Opal.find_super_dispatcher(this, 'initialize', initialize), [backend, opts])
    this[$precedingAttributes] = precedingAttributes
  })
  Opal.defn(scope, '$convert', function $$convert (opts) {
    var self = this
    self.parent_document.$playback_attributes(self[$precedingAttributes])
    return Opal.send(this, Opal.find_super_dispatcher(this, 'convert', $$convert), [opts])
  })
  return scope
})()

const inlineDoc = (() => {
  const scope = Opal.klass(
    Opal.module(null, 'Templates', function () {
    }),
    Opal.module(null, 'Asciidoctor').Inline,
    'inlineDoc',
    function () {
    }
  )
  Opal.defn(scope, '$initialize', function initialize (parent, innerDoc, precedingAttributes) {
    Opal.send(this, Opal.find_super_dispatcher(this, 'initialize', initialize), [parent, '', '', null])
    this[$innerDoc] = innerDoc
    this[$precedingAttributes] = precedingAttributes
  })
  Opal.defn(scope, '$convert', function $$convert (opts) {
    var self = this
    self.document.$playback_attributes(self[$precedingAttributes])
    return self[$innerDoc].$convert()
  })
  return scope
})()

const attributeEntry = Opal.module(null, 'Asciidoctor').Document.AttributeEntry

module.exports.register = function (registry, config = {}) {
  // TODO (thanks to Ewan)
  //
  // both positional and named attributes. DONE
  // external attributes not conflicting with positional attributes are available in the template DONE
  // all positional attributes block external attributes; if not specified they are set to undefined. DONE
  // inline macro. Done
  // consider shortening the names to template maybe not, since the "apply" uses the supplied name.
  // Investigate creating the blockmacro/inlinemacro during defineTemplate. Done!

  function blockMacroTemplate () {
    template(this, 'blockMacroTemplate', (name, parameters, template) => {
      //after initialization, the global extensions are in a registry, and we have to register in that one.
      if (typeof registry.blockMacro === 'function') {
        registry.blockMacro(applyBlockMacroTemplate(name, parameters, template))
      } else {
        console.warn('no \'blockMacro\' method on alleged registry')
      }
      //return undefined to leave the template definition out of the rendered document.
    })
  }

  function blockTemplate () {
    template(this, 'blockTemplate', (name, parameters, template) => {
      //after initialization, the global extensions are in a registry, and we have to register in that one.
      if (typeof registry.block === 'function') {
        registry.block(applyBlockTemplate(name, parameters, template))
      }
      //return undefined to leave the template definition out of the rendered document.
    })
  }

  function inlineTemplate () {
    template(this, 'inlineTemplate', (name, parameters, template) => {
      //after initialization, the global extensions are in a registry, and we have to register in that one.
      if (typeof registry.inlineMacro === 'function') {
        registry.inlineMacro(applyInlineTemplate(name, parameters, template))
      } else {
        console.warn('no \'inlineMacro\' method on alleged registry')
      }
      //return undefined to leave the template definition out of the rendered document.
    })
  }

  function template (self, defineTemplateName, register) {
    self.named(defineTemplateName)
    self.onContext(['listing', 'open', 'paragraph'])
    self.positionalAttributes(['name', 'parameters'])
    self.process(function (parent, reader, attrs) {
      const name = attrs.name
      const parameters = attrs.parameters ? attrs.parameters.split(',') : []
      const template = reader.$read()
      //after initialization, the global extensions are in a registry, and we have to register in that one.
      register(name, parameters, template)
      //return undefined to leave the template definition out of the rendered document.
    })
  }

  function prepareAttributes (attributes, parent) {
    //precedingAttributeEntries are the attributes defined between the previous block and this block
    const precedingAttributeEntries = attributes.attribute_entries
    delete attributes.attribute_entries
    const precedingAttributes = Opal.hash2(['attribute_entries'], { attribute_entries: precedingAttributeEntries })
    //Turn attributes into a hash suitable for playbackAttributes
    const attrHash = Opal.hash2(['attribute_entries'],
      {
        attribute_entries: Object.entries(attributes).map(([name, value]) => {
          return attributeEntry.$new(name, value, value === undefined)
        }),
      })

    parent.document.$playback_attributes(precedingAttributes)
    //Save attributes up to here, including preceding attributes
    const originalAttributes = parent.document.$attributes().$merge(EMPTY)
    //apply attributes for this template
    parent.document.$playback_attributes(attrHash)
    return { precedingAttributes, originalAttributes }
  }

  function applySubs (parent, attributes, template, type, name) {
    const subs = attributes.subs || 'attributes'
    template = parent.applySubstitutions(template, parent.$resolve_subs(subs, type, ['attributes'], name))
    return template
  }

  function makeInnerDoc (parent, template, precedingAttributes, originalAttributes) {
    const options = Opal.hash2(['attributes', 'standalone', 'extension_registry', 'parent'],
      {
        standalone: false,
        extension_registry: registry,
        parent: parent.document,
      })
    const innerDoc = innerDocument.$new(template, options, precedingAttributes).parse()
    parent.document.attributes = originalAttributes
    return innerDoc
  }

  function makeInlineDoc (parent, template, originalAttributes, precedingAttributes) {
    const options = Opal.hash2(['attributes', 'doctype', 'standalone', 'extension_registry'],
      {
        attributes: parent.document.$attributes().$merge(EMPTY),
        doctype: 'inline',
        standalone: false,
        extension_registry: registry,
      })
    const innerDoc = inlineDocument.$new(template, options).parse()
    parent.document.attributes = originalAttributes
    const inlineBlock = inlineDoc.$new(parent, innerDoc, precedingAttributes)
    return inlineBlock
  }

  function applyBlockMacroTemplate (name, positionalParameters, template) {
    return function () {
      var self = this
      self.named(name)
      self.positionalAttributes(positionalParameters)
      self.$option('format', 'short')
      self.process((parent, target, attributes) => {
        const { precedingAttributes, originalAttributes } = prepareAttributes(attributes, parent)
        const substitutedTemplate = applySubs(parent, attributes, template, 'block', name)
        return makeInnerDoc(parent, substitutedTemplate, precedingAttributes, originalAttributes)
      })
    }
  }

  function applyBlockTemplate (name, positionalParameters, template) {
    return function () {
      var self = this
      self.named(name)
      self.onContext(['listing', 'open', 'paragraph'])
      self.positionalAttributes(positionalParameters)
      self.process((parent, reader, attributes) => {
        const content = reader.$read()
        attributes.content = content
        //For some reason block processors don't get nil for missing positional parameters, so we set them.
        positionalParameters.forEach((name) => {
          if (!(name in attributes)) {
            attributes[name] = undefined
          }
        })
        const { precedingAttributes, originalAttributes } = prepareAttributes(attributes, parent)
        const substitutedTemplate = applySubs(parent, attributes, template, 'block', name)
        //Returning the inner doc seems to cause it to be processed as lines, which it doesn't have.
        // return makeInnerDoc(parent, subbedTemplate, precedingAttributes, originalAttributes)
        parent.blocks.push(makeInnerDoc(parent, substitutedTemplate, precedingAttributes, originalAttributes))
      })
    }
  }

  function applyInlineTemplate (name, positionalParameters, template) {
    return function () {
      var self = this
      self.named(name)
      self.positionalAttributes(positionalParameters)
      self.$option('format', 'short')
      self.process((parent, target, attributes) => {
        const { precedingAttributes, originalAttributes } = prepareAttributes(attributes, parent)
        const substitutedTemplate = applySubs(parent, attributes, template, 'inline', name)
        return makeInlineDoc(parent, substitutedTemplate, originalAttributes, precedingAttributes)
      })
    }
  }

  function doRegister (registry) {
    if (typeof registry.block === 'function') {
      registry.block(blockMacroTemplate)
      registry.block(blockTemplate)
      registry.block(inlineTemplate)
    } else {
      console.warn('no \'block\' method on alleged registry')
    }
  }

  if (typeof registry.register === 'function') {
    registry.register(function () {
      //Capture the global registry so processors can register more extensions.
      registry = this
      doRegister(registry)
    })
  } else {
    doRegister(registry)
  }
  return registry
}
