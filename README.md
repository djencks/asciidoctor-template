# Asciidoctor.js capturing templates
:version: 0.0.3

`@djencks/asciidoctor-template` provides an Asciidoctor.js extension to "capture" asciidoc templates, with "variable" parameters, and register block macro, block, or inline macro templates to reproduce the content, with provided parameter values.
The parameter values are isolated from the outside document.

NOTE: for more complete, better formatted README, see https://gitlab.com/djencks/asciidoctor-template/-/blob/master/README.adoc.

WARNING: The `blockTemplate` processor from version `0.0.1` has been renamed more accurately `blockMacroTemplate` and a new `blockTemplate` processor added.

## Installation

Available through npm as @djencks/asciidoctor-template.

The project git repository is https://gitlab.com/djencks/asciidoctor-template

## Usage in asciidoctor.js

see https://gitlab.com/djencks/asciidoctor-template/-/blob/master/README.adoc

## Usage in Antora

see https://gitlab.com/djencks/asciidoctor-template/-/blob/master/README.adoc

## Antora Example project

An example project showing some uses of this extension is under extensions/template-extension in `https://gitlab.com/djencks/simple-examples`.
